package com.w4cms.core;



import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.ext.handler.FakeStaticHandler;
import com.jfinal.ext.handler.RenderingTimeHandler;
import com.jfinal.ext.plugin.config.ConfigKit;
import com.jfinal.ext.plugin.config.ConfigPlugin;
import com.jfinal.ext.plugin.tablebind.AutoTableBindPlugin;
import com.jfinal.ext.plugin.tablebind.SimpleNameStyles;
import com.jfinal.ext.route.AutoBindRoutes;
import com.jfinal.plugin.activerecord.SqlReporter;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.w4cms.utils.HttlRenderFactory;
public class WebConfig extends JFinalConfig {

	@Override
	public void configConstant(Constants me) {
		me.setDevMode(true);
		new ConfigPlugin(".*.txt").reload(false).start();
        me.setMainRenderFactory(new HttlRenderFactory());		// 设置httl模板
        //me.setError404View("/error/403.html");
        me.setError404View("/error/404.html");
        //me.setError500View("/error/500.html");
	}

	@Override
	public void configRoute(Routes me) {
		me.add(new AutoBindRoutes().autoScan(false));
		/*
		me.add(new AdminRoutes());
		me.add(new IndexRoutes());
		me.add(new UserRoutes());
		*/
	}

	@Override
	public void configPlugin(Plugins me) {
		/*
		loadPropertyFile("classes/config.properties");
		C3p0Plugin c3p0Plugin = new C3p0Plugin(getProperty("jdbc.url"),getProperty("jdbc.username"),getProperty("jdbc.password"),getProperty("jdbc.driverClassName"));
		*/
		C3p0Plugin c3p0Plugin = new C3p0Plugin(ConfigKit.getStr("jdbc.url"),ConfigKit.getStr("jdbc.username"),ConfigKit.getStr("jdbc.password"),ConfigKit.getStr("jdbc.driverClassName"));
		me.add(c3p0Plugin);
		AutoTableBindPlugin atbp = new AutoTableBindPlugin(c3p0Plugin, SimpleNameStyles.LOWER_UNDERLINE);
        //atbp.addExcludeClasses(DbModel.class);
        atbp.setShowSql(true);
        me.add(atbp);
        // sql记录
        SqlReporter.setLogger(true);
	}

	@Override
	public void configInterceptor(Interceptors me) {

	}

	@Override
	public void configHandler(Handlers me) {
		me.add(new FakeStaticHandler());
		//me.add(new HttpCacheHandler());
		me.add(new ContextPathHandler("path"));
		me.add(new RenderingTimeHandler());
	}

}
