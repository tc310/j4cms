package com.w4cms.model;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.plugin.activerecord.Model;
import com.w4cms.utils.Tools;

public class Category extends Model<Category>{
	private static final long serialVersionUID = 485724669113236284L;
	public static final Category dao = new Category();
	public List<TreeData> cateTree(Object pid){
		List<TreeData> list = new ArrayList<TreeData>();
		List<Category> objs = null;
		if(pid!=null){
			objs = dao.find("select id,name,parent_id as pId from category where status!=-1 and parent_id=? order by cate_order asc", pid);
		}else{
			objs = dao.find("select id,name,parent_id as pId from category where status!=-1 order by cate_order asc");
		}
		
		for (Category category : objs) {
			TreeData data = new TreeData();
			data.setId(category.get("id").toString());
			data.setPid(category.get("pId").toString());
			data.setText(category.getStr("name"));
			list.add(data);
		}
		return TreeData.formatTree3(list);
	}
	public List<TreeData> searchCategoryTree(String search){
		List<TreeData> list = new ArrayList<TreeData>();
		List<Category> ms = null;
		if(Tools.isEmpty(search))
			ms = dao.find("select * from category where status!=-1 order by cate_order asc");
		else
			ms = dao.find("select * from category where status!=-1 and name like ? order by cate_order asc","%"+search+"%");
		for (Category category : ms) {
			TreeData data = new TreeData();
			data.setId(category.getStr("id").toString());
			data.setPid(category.getStr("parent_id").toString());
			data.setText(category.getStr("name"));
			data.getAttributes().put("status", category.get("status"));
			data.getAttributes().put("remark", category.get("remark"));
			data.getAttributes().put("cate_order", category.get("cate_order"));
			data.getAttributes().put("create_time", category.get("create_time"));
			data.getAttributes().put("update_time", category.get("update_time"));
			list.add(data);
		}
		return TreeData.formatTree3(list);
	}
}
