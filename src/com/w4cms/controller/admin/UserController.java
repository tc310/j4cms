package com.w4cms.controller.admin;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;
import com.jfinal.plugin.activerecord.Page;
import com.w4cms.interceptor.AdminInterceptor;
import com.w4cms.model.SysMenu;
import com.w4cms.model.SysRole;
import com.w4cms.model.SysUser;
import com.w4cms.utils.PasswordUtil;
import com.w4cms.utils.RightsUtils;
import com.w4cms.utils.Tools;
@ControllerBind(controllerKey="/admin/user",viewPath="admin/user")
@Before(AdminInterceptor.class)
public class UserController extends Controller {
	public void list(){
		this.search();
	}
	public void add(){
		setAttr("roles", SysRole.dao.getRoleListById(null));
		setAttr("menuNodes", JSONArray.toJSONString(SysMenu.dao.getMenuList(null)));
		render("userinfo.html");
	}
	
	public void edit(){
		Object id = getPara();
		SysUser user = SysUser.dao.findById(id);
		if(user==null) {
			render("用户不存在.");
			return;
		}
		setAttr("user", user);
		String rights = user.getStr("rights");
		List<Map<String,Object>> list = SysMenu.dao.getMenuList(null);
		for(Map<String,Object> menu : list){
			Integer mid =(Integer)menu.get("id");
			if(RightsUtils.testRights(rights, mid)){
				menu.put("checked", true);
			}
		}
		setAttr("menuNodes", JSONArray.toJSONString(list));
		setAttr("roles", SysRole.dao.getRoleListById(null));
		render("userinfo.html");
	}
	
	public void save(){
		String id = getPara("id");
		SysUser u = new SysUser();
		if((Tools.isEmpty(getPara("password")) && Tools.isEmpty(id)) || Tools.isEmpty(getPara("username"))){
			renderJson("msg", "用户名或者密码为空.");
			return;
		}else if(!Tools.isEmpty(getPara("password")) && !Tools.isEmpty(id)){
			u.set("password", PasswordUtil.generate(getPara("password")));
		}
		u.set("username", getPara("username"));
		u.set("email", getPara("email"));
		u.set("sex", getPara("sex"));
		u.set("role_id",getPara("role"));
		u.set("rights",RightsUtils.sumRights(getPara("rights").split(",")));
		boolean isOk = false;
		if(!Tools.isEmpty(id))
		{
			u.set("id", id);
			isOk = u.updateUser();
		}else{
			isOk = u.saveUser();
		}
		if(isOk){
			renderJson("success", true);
		}else{
			renderJson("msg", "操作账户失败.");
		}
	}
	public void resetpwd(){
		Object id = getPara();
		SysUser u = new SysUser();
		u.set("id", id);
		u.set("password", PasswordUtil.generate("123456"));
		boolean isOk = u.updateUser();
		if(isOk){
			renderText("true");
		}else{
			renderText("修改密码失败.");
		}
	}
	
	public void delete(){
		Object id= getPara();
		SysUser u = new SysUser();
		u.set("id", id);
		u.set("status", -1);
		boolean isOk = u.updateUser();
		if(isOk){
			renderText("true");
		}else{
			renderText("删除账户失败.");
		}
	}
	
	public void search(){
		String search = getPara("search");
		String role = getPara("role");
		Integer pageNumber = getParaToInt("p",1);
		if(pageNumber<1){
			pageNumber=1;
		}
		Integer pageSize = getParaToInt("r",15);
		if(pageSize<1 || pageSize>100){
			pageSize = 15;
		}
		Page<SysUser> list = SysUser.dao.searchUsers(pageNumber, pageSize, search, role);
		setAttr("users", list);
		setAttr("roles", SysRole.dao.getRoleListById(null));
		setAttr("pageStr", Tools.getPageStr(list));
		keepPara();
		render("userlist.html");
	}
}
