//@charset "utf-8";
/*
(function(config){
    //config['extendDrag'] = true; // 注意，此配置参数只能在这里使用全局配置，在调用窗口的传参数使用无效
    config['lock'] = true;
    config['fixed'] = true;
    config['min'] = false;
    config['max'] = false;
    config['okVal'] = '确定';
    config['cancelVal'] = '取消';
})($.dialog.setting);
*/
//$.dialog.setting.extendDrag = true;
//var path = $.dialog.setting.path;
var inputError = function (input) {
	clearTimeout(inputError.timer);
	var num = 0;
	var fn = function () {
		inputError.timer = setTimeout(function () {
			input.className = input.className === '' ? 'login-form-error' : '';
			if (num === 5) {
				input.className === '';
			} else {
				fn(num ++);
			};
		}, 150);
	};
	fn();
};
function addUser(){
	api = $.dialog({id:'msgDialogAdd',title: '添加用户',ok:function(){
		$("#addform").submit();
		return false;
	},cancel:function(){
		return true;
	}});
	$.ajax({
		url:'add.html',
		success:function(data){
			api.content(data);
		},
		cache:false
	});
}
function editUser(){
	var chk = $("input[name='id']:checked")[0];
	if(chk){
		api = $.dialog({id:'msgDialogUpdate',title: '修改用户',ok:function(){
			$("#addform").submit();
			return false;
		},cancel:function(){
			return true;
		}});
		$.ajax({
			url:'edit/'+$(chk).val(),
			success:function(data){
				api.content(data);
			},
			cache:false
		});
	}else{
		$.dialog({title:'提示',content:'请选择一位用户.'}).time(2);
	}
}
function reloadMe(t){
	if(isNaN(t)){
		try {
			document.forms[0].submit();
		} catch (e) {
		}
	}
	else{
		setTimeout(function(){
			document.forms[0].submit();
		},t);
	}
}

$(function(){
	$("#add").click(function(e){
		addUser();
	});
	$("#edit").click(function(e){
		editUser();
	});
	$(".resetpwd").click(function(){
		var id = $(this).attr('data-input');
		$.dialog.confirm("您确定要将该用户密码重置为123456?",function(){
			$.ajax({
				url:'resetpwd/'+id,
				dataType:'text',
				success:function(d){
					if(d=='true'){
						$.dialog({title:'提示',content:'操作成功!'}).time(1);
					}else{
						$.dialog({title:'提示',content:d}).time(3);
					}
					reloadMe(2000);
				},
				error:function(){
					$.dialog({title:'提示',content:'操作失败'}).time(1);
					reloadMe(1000);
				}
			});
		});
	});
	$(".delete").click(function(e){
		e.stopPropagation();
		var id = $(this).attr('data-input');
		$.dialog.confirm("您确定要删除该条数据吗??",function(){
			$.ajax({
				url:'delete/'+id,
				dataType:'text',
				success:function(d){
					if(d=='true'){
						$.dialog({title:'提示',content:'操作成功!'}).time(1);
					}else{
						$.dialog({title:'提示',content:d}).time(3);
					}
				},
				error:function(){
					$.dialog({title:'提示',content:'操作失败'}).time(1);
				}
			});
		});
	});
	$("#chkAll").change(function(e){
		e.stopPropagation();
		if($(this).attr("checked")){
			$("input[name='id']").attr("checked",true);
		}else{
			$("input[name='id']").attr("checked",false);
		}
		$("input[name='id']").change();
	});
	$("input:checkbox").click(function(e){
		e.stopPropagation();
	});
	$(".table").hcheckbox();
});